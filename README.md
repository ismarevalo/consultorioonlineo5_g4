<h1>ConsultorioOnlineO5_G4</h1>
![UIS_LOGO.png](./UIS_LOGO.png)
<h1>JAVA proyecto usando modelo MVC y base de datos MySQL</h1>
<h2>Autores : Ismael Arévalo González & Germán Arévalo Hernández</h2>
<h2>Profesora : Yhary Estefania Arias</h2>
<h3>Descripción :</h3>
<h4>Esta es una aplicación códificada en JAVA cuyo propósito es gestionar un consultorio médico, gestionar los médicos, especialidades, historias clinicas, medicamentos y citas con una base de datos MySQL usando la estructura Modelo, Vista, Controlador (MVC).</h4>

![ConsultorioFondo.jpeg](./ConsultorioFondo.jpeg)

<h3>Funcionalidades :</h3>
<h4>
- Se puede parametrizar llos médicos, pacientes y citas médicas<br>
- CRUD en médico y paciente<br>
- Un paciente puede crear, borrar, modificar y consultar citas médicas</h4>
