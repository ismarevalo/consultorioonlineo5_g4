package mintic;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping; // Nos indica las rutas. es de consulta
import org.springframework.web.bind.annotation.RequestParam; //MVC modelos vista controlador sirve para acceder al valor de uno de los parametros. Se usa en las consultas (GET)
import org.springframework.web.bind.annotation.RestController; // Es un componente de springboot recibe peticiones HTTP y las responde. 
import com.google.gson.Gson; // Json - Cuando hacemos una peticion a un servicio el nos retorna una json
import mintic.modelo.Cita_medica;
import mintic.modelo.Medico;
import mintic.modelo.Paciente;
import org.springframework.http.MediaType; // Es un paque para esas consultas o servicio
import org.springframework.web.bind.annotation.DeleteMapping; // indicarle donde vamos a eliminar. Es una ruta donde indicamos lo que va eliminar y donde eliminar. //Delete para eliminar
import org.springframework.web.bind.annotation.PathVariable; //
import org.springframework.web.bind.annotation.PostMapping; // es para hacer una actualizacion. El Post actualizar - Insertar // Put Insertar
import org.springframework.web.bind.annotation.RequestBody; //

/*import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;*/
@SpringBootApplication // Donde se encuentre este arroba, es el Main.
@RestController // Indica que la clase será un API REST.
public class ConsultorioOnLine05G4SpringBootApplication {

    @Autowired(required = true) // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a p = new Paciente();
    Paciente p; // Instancias - Objeto 
    @Autowired(required = true)
    Medico m; // Instancia
    @Autowired(required = true)
    Cita_medica c; // Instancia
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.

    public static void main(String[] args) {
        SpringApplication.run(ConsultorioOnLine05G4SpringBootApplication.class, args);
    }

// EJEMPLO
    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "20") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);
    }

    @GetMapping("/Paciente")
    public String consultarPacientePorId(@RequestParam(value = "idpaciente", defaultValue = "0") int idpaciente) throws ClassNotFoundException, SQLException {
        p.setIdpaciente(idpaciente); // Pasamos id al metodo setId y luego validamos si existe o no 
        if (p.consultar()) { // True
            String res = new Gson().toJson(p); // se crea un json vacio
            p.setIdpaciente(0); // Si hay más registros SOLO traigase el primero que encuentre
            p.setNombre("");
            p.setDocumento(0);
            return res;
        } else { // False           
            return new Gson().toJson(p);
        }
    }

    // Consultar por un solo medico.
    @GetMapping("/Medico")
    public String consultarPacientePorIdMedico(@RequestParam(value = "idmedico", defaultValue = "0") int idmedico) throws ClassNotFoundException, SQLException {
        m.setIdmedico(idmedico);
        if (m.consultar()) {
            String res = new Gson().toJson(m);
            m.setIdmedico(0); // Si hay más registros SOLO traigase el primero que encuentre
            m.setNombre("");
            m.setEspecialidad("");
            return res;
        } else {
            return new Gson().toJson(m);
        }
    }

    @GetMapping("/Citamedica")
    public String consultarCitaMedicaPorIdCitaMedica(@RequestParam(value = "idcita_medica", defaultValue = "0") int idcita_medica) throws ClassNotFoundException, SQLException {
        List<Cita_medica> citas = c.consultarTodo(idcita_medica);
        if (!citas.isEmpty()) {
            return new Gson().toJson(citas);

        } else {
            return new Gson().toJson(citas);

        }
    }

    // GET - POST - PUT - DELETE
    @PostMapping(path = "/Paciente", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición

    // RequestBody sirve para hacer actualizaciones y guardar datos nuevos en la base de datos.
    public String actualizarPaciente(@RequestBody String Paciente) throws ClassNotFoundException, SQLException {
        Paciente pa = new Gson().fromJson(Paciente, Paciente.class); // recibimos el json y lo devolvemos un objeto estudiante
        p.setIdpaciente(pa.getIdpaciente()); // obtengo el id de f y se lo ingreso e
        p.setNombre(pa.getNombre());
        p.setDocumento(pa.getDocumento());
        /*p.setEmail(pa.getEmail());
        p.setTelefono(pa.getTelefono());
        p.setDireccion(pa.getDireccion());
        p.setEps(pa.getEps()); */
        p.actualizar();
        return new Gson().toJson(p); // cliente le envia json al servidor. fpr de comunicarse

    }
    
    // Guardar Paciente    
    public String insertarPaciente(@RequestBody String Paciente) throws ClassNotFoundException, SQLException {
        Paciente pacientes = new Gson().fromJson(Paciente, Paciente.class);
        pacientes.setIdpaciente(pacientes.getIdpaciente()); 
        pacientes.setNombre(pacientes.getNombre());
        pacientes.setDocumento(pacientes.getDocumento());
        pacientes.setEmail(pacientes.getEmail());
        pacientes.setTelefono(pacientes.getTelefono());
        pacientes.setDireccion(pacientes.getDireccion());
        pacientes.setEps(pacientes.getEps());
        pacientes.Guardar(); 
        return new Gson().toJson(pacientes);
    }
    
    // PathVarible sirve para recibir un Id y es para eliminar
    @DeleteMapping("/eliminarCita_medica/{idcita_medica}")
    public String borrarCitamedica(@PathVariable("idcita_medica") int idcita_medica) throws SQLException, ClassNotFoundException {
        c.setIdcita_medica(idcita_medica);
        c.borrar();
        return "Los datos del id indicado han sido eliminados";
    }
}