package mintic.modelo;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @autores Germán Hernández e Ismael Arévalo - Versión final
 */
@Component("medico")
public class Medico {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

    // Atributos
    private int idmedico;
    private String nombre;
    private String especialidad;

    // Contructor
    public Medico(int idmedico, String nombre, String especialidad) {
        this.idmedico = idmedico;
        this.nombre = nombre;
        this.especialidad = especialidad;
    }

    public Medico() {
    }

    // Metodos
    public int getIdmedico() {
        return idmedico;
    }

    public void setIdmedico(int idmedico) {
        this.idmedico = idmedico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public String toString() {
        return "Medico{" + "idmedico=" + idmedico + ", nombre=" + nombre + ", especialidad=" + especialidad + '}';
    }

    // CRUD - C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO medico(idmedico, nombre, especialidad) VALUES(?,?,?)";
        return sql;
    }
    
    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idmedico,nombre,especialidad FROM medico WHERE idmedico = ?";
        List<Medico> medico = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Medico(
                        rs.getInt("idmedico"),
                        rs.getString("nombre"),
                        rs.getString("especialidad")
                ), new Object[]{this.getIdmedico()});
        if (medico != null && !medico.isEmpty()) {
            this.setIdmedico(medico.get(0).getIdmedico());
            this.setNombre(medico.get(0).getNombre());
            this.setEspecialidad(medico.get(0).getEspecialidad());

            return true;
        } else {
            return false;
        }
    }

    public List<Medico> consultarTodo(int idmedico) throws ClassNotFoundException, SQLException {
        String sql = "SELECT idmedico,nombre,especialidad FROM medico WHERE idmedico = ?";
        List<Medico> medico = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Medico(
                        rs.getInt("idmedico"), 
                        rs.getString("nombre"), 
                        rs.getString("especialidad")
                ), new Object[]{this.getIdmedico()});
        return medico;
    }

    // CRUD - U
    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE medico SET nombre = ?, especialidad = ? WHERE idmedico = ?";
        return sql;
    }

    // CRUD - D
    public boolean borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM medico WHERE idmedico = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getIdmedico());
        ps.execute();
        ps.close();
        return true;
    }
}