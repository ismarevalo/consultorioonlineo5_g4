package mintic.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @autores Germán Hernández e Ismael Arévalo - Versión final
 */
@Component("cita_medica")

public class Cita_medica {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

    // Atributos
    private int idcita_medica;
    private String tipo;
    private float valor;
    private String fecha;
    private Medico fk_idmedico;
    private Paciente fk_idpaciente;

    // Contructor
    public Cita_medica(int idcita_medica, String tipo, float valor, String fecha) {
        this.idcita_medica = idcita_medica;
        this.tipo = tipo;
        this.valor = valor;
        this.fecha = fecha;
    }

    public Cita_medica() {
    }

    // Metodos
    public int getIdcita_medica() {
        return idcita_medica;
    }

    public void setIdcita_medica(int idcita_medica) {
        this.idcita_medica = idcita_medica;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    // si surge algún error cambiar los atributos de los set con el tipo de dato correcto (int, String)
    // También debemos cambiar el return por fk_idmedico.getIdMedico
    public Medico getFk_idmedico() {
        return fk_idmedico;
    }

    public void setFk_idmedico(Medico fk_idmedico) {
        this.fk_idmedico = fk_idmedico;
    }

    public Paciente getFk_idpaciente() {
        return fk_idpaciente;
    }

    public void setFk_idpaciente(Paciente fk_idpaciente) {
        this.fk_idpaciente = fk_idpaciente;
    }

    @Override
    public String toString() {
        return "Cita_medica{" + "idcita_medica=" + idcita_medica + ", tipo=" + tipo + ", valor=" + valor + ", fecha=" + fecha + ", fk_idmedico=" + fk_idmedico + ", fk_idpaciente=" + fk_idpaciente + '}';
    }

    // CRUD - C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO cita_medica(idcita_medica, tipo, valor, fecha) VALUES(?,?,?,?)";
        return sql;

    }
    
    // Consultar
    public List<Cita_medica> consultarCitaPaciente() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idcita_medica,tipo,valor,fecha FROM cita_medica WHERE fk_idpaciente = ?";
        List<Cita_medica> cita_paciente = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Cita_medica(
                        rs.getInt("idcita_medica"),
                        rs.getString("tipo"),
                        rs.getFloat("valor"),
                        rs.getString("fecha")
                ), new Object[]{this.getFk_idpaciente()});

        return cita_paciente;
    }

    public List<Cita_medica> consultarCitaMedico() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idcita_medica,tipo,valor,fecha FROM cita_medica WHERE fk_idmedico = ?";
        List<Cita_medica> cita_medico = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Cita_medica(
                        rs.getInt("idcita_medica"),
                        rs.getString("tipo"),
                        rs.getFloat("valor"),
                        rs.getString("fecha")
                ), new Object[]{this.getFk_idmedico()});
        return cita_medico;
    }

    public List<Cita_medica> consultarTodo(int idpaciente) throws ClassNotFoundException, SQLException {
        String sql = "SELECT idcita_medica,tipo,valor,fecha,idpaciente FROM cita_medica WHERE idpaciente = ?";
        List<Cita_medica> cita = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Cita_medica(
                        rs.getInt("idcita_medica"),
                        rs.getString("tipo"),
                        rs.getFloat("valor"),
                        rs.getString("fecha")
                ), new Object[]{this.getFk_idpaciente(), this.getFk_idmedico()});
        return cita;
    }

    // CRUD - U
    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE cita_medica SET idcita_medica = ?, tipo = ?, valor = ?, fecha = ? WHERE idpaciente = ?";
        return sql;
    }

    // CRUD - D
    public boolean borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM cita_medica WHERE idcita_medica = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getIdcita_medica());
        ps.execute();
        ps.close();
        return true;
    }
}
