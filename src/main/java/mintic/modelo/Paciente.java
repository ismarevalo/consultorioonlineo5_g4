package mintic.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @autores Germán Hernández e Ismael Arévalo - Versión final
 */
@Component("paciente")

public class Paciente {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

    // Atributos
    private int idpaciente;
    private String nombre;
    private int documento;
    private String email;
    private String telefono;
    private String direccion;
    private String eps;

    // Constructor
    public Paciente(int idpaciente, String nombre, int documento, String email, String telefono, String direccion, String eps) {
        this.idpaciente = idpaciente;
        this.nombre = nombre;
        this.documento = documento;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.eps = eps;
    }

    public Paciente() {
    }

    // Atributos
    public int getIdpaciente() {
        return idpaciente;
    }

    public void setIdpaciente(int idpaciente) {
        this.idpaciente = idpaciente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    @Override
    public String toString() {
        return "Paciente{" + "idpaciente=" + idpaciente + ", nombre=" + nombre + ", documento=" + documento + ", email=" + email + ", telefono=" + telefono + ", direccion=" + direccion + ", eps=" + eps + '}';
    }

    // CRUD - C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO paciente(idpaciente, nombre, documento, email, telefono, direccion, eps) VALUES(?,?,?,?,?,?,?)";
        return sql;
    }
    
    // Guardar Paciente
    public int Guardar() throws ClassNotFoundException, SQLException {
        int last_inserted_idpaciente = -1;
        String sql = "INSERT INTO Paciente(idpaciente, nombre, documento, email, telefono, direccion, eps) VALUES(?,?,?,?,?,?,?)";
        Connection paciente = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = paciente.prepareStatement(sql);
        ps.setInt(1, this.getIdpaciente());
        ps.setString(2, this.getNombre());
        ps.setInt(3, this.getDocumento());
        ps.setString(4, this.getEmail());
        ps.setString(5, this.getTelefono());
        ps.setString(6, this.getDireccion());
        ps.setString(7, this.getEps());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys(); // llave generada del PS -- insertar con ID
        if (rs.next()) {
            last_inserted_idpaciente = rs.getInt(1);
        }
        ps.close();
        return last_inserted_idpaciente;
    }


    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idpaciente,nombre,documento,email,telefono,direccion,eps FROM paciente WHERE idpaciente = ?";
        List<Paciente> paciente = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Paciente(
                        rs.getInt("idpaciente"),
                        rs.getString("nombre"),
                        rs.getInt("documento"),
                        rs.getString("email"),
                        rs.getString("telefono"),
                        rs.getString("direccion"),
                        rs.getString("eps")
                ), new Object[]{this.getIdpaciente()});
        if (paciente != null && !paciente.isEmpty()) {
            this.setIdpaciente(paciente.get(0).getIdpaciente());
            this.setNombre(paciente.get(0).getNombre());
            this.setDocumento(paciente.get(0).getDocumento());
            this.setEmail(paciente.get(0).getEmail());
            this.setTelefono(paciente.get(0).getTelefono());
            this.setDireccion(paciente.get(0).getDireccion());
            this.setEps(paciente.get(0).getEps());

            return true;
        } else {
            return false;
        }
    }

    public List<Paciente> consultarTodo(int idpaciente) throws ClassNotFoundException, SQLException {
        String sql = "SELECT idpaciente,nombre,documento,email,telefono,direccion,eps FROM paciente WHERE idpaciente = ?";
        List<Paciente> paciente = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Paciente(
                        rs.getInt("idpaciente"), 
                        rs.getString("nombre"), 
                        rs.getInt("documento"),
                        rs.getString("email"),
                        rs.getString("telefono"),
                        rs.getString("direccion"),
                        rs.getString("eps")
                ), new Object[]{this.getIdpaciente()});
        return paciente;
    }

    // CRUD - U
    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE paciente SET nombre = ?, documento = ?, email = ?, telefono = ?, direccion = ?, eps = ? WHERE idpaciente = ?";
        return sql;
    }

}