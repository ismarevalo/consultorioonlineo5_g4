package com.example.ConsultorioOnline05_G4;

import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.List;
import mintic.modelo.Cita_medica;
import mintic.modelo.Medico;
import mintic.modelo.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @autores Germán Hernández e Ismael Arévalo - Versión final
 */

@SpringBootApplication // Donde se encuentre este arroba, es el Main.
@RestController
public class ConsultorioOnline05G4Application {

    @Autowired(required = true) // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a p = new Paciente();
    Paciente p; // Instancias - Objeto 
    @Autowired(required = true)
    Medico m; // Instancia
    @Autowired(required = true)
    Cita_medica c; // Instancia
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.

    public static void main(String[] args) {
        SpringApplication.run(ConsultorioOnline05G4Application.class, args);
    }
    
    // EJEMPLO
    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "20") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);
    }

    @GetMapping("/Paciente")
    public String consultarPacientePorId(@RequestParam(value = "idpaciente", defaultValue = "0") int idpaciente) throws ClassNotFoundException, SQLException {
        p.setIdpaciente(idpaciente); // Pasamos id al metodo setId y luego validamos si existe o no 
        if (p.consultar()) { // True
            String res = new Gson().toJson(p); // se crea un json vacio
            p.setIdpaciente(0); // Si hay más registros SOLO traigase el primero que encuentre
            p.setNombre("");
            p.setDocumento(0);
            return res;
        } else { // False           
            return new Gson().toJson(p);
        }
    }

    // Consultar por un solo medico.
    @GetMapping("/Medico")
    public String consultarPacientePorIdMedico(@RequestParam(value = "idmedico", defaultValue = "0") int idmedico) throws ClassNotFoundException, SQLException {
        m.setIdmedico(idmedico);
        if (m.consultar()) {
            String res = new Gson().toJson(m);
            m.setIdmedico(0); // Si hay más registros SOLO traigase el primero que encuentre
            m.setNombre("");
            m.setEspecialidad("");
            return res;
        } else {
            return new Gson().toJson(m);
        }
    }

    @GetMapping("/Citamedica")
    public String consultarCitaMedicaPorIdCitaMedica(@RequestParam(value = "idcita_medica", defaultValue = "0") int idcita_medica) throws ClassNotFoundException, SQLException {
        List<Cita_medica> citas = c.consultarTodo(idcita_medica);
        if (!citas.isEmpty()) {
            return new Gson().toJson(citas);

        } else {
            return new Gson().toJson(citas);

        }
    }

    // GET - POST - PUT - DELETE
    @PostMapping(path = "/Paciente", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición

    // RequestBody sirve para hacer actualizaciones y guardar datos nuevos en la base de datos.
    public String actualizarPaciente(@RequestBody String Paciente) throws ClassNotFoundException, SQLException {
        Paciente pa = new Gson().fromJson(Paciente, Paciente.class); // recibimos el json y lo devolvemos un objeto estudiante
        p.setIdpaciente(pa.getIdpaciente()); // obtengo el id de f y se lo ingreso e
        p.setNombre(pa.getNombre());
        p.setDocumento(pa.getDocumento());
        /*p.setEmail(pa.getEmail());
        p.setTelefono(pa.getTelefono());
        p.setDireccion(pa.getDireccion());
        p.setEps(pa.getEps()); */
        p.actualizar();
        return new Gson().toJson(p); // cliente le envia json al servidor. fpr de comunicarse

    }
    
    // Guardar Paciente 
    @PostMapping(path = "/GuardarPaciente", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String insertarPaciente(@RequestBody String Paciente) throws ClassNotFoundException, SQLException {
        Paciente pacientes = new Gson().fromJson(Paciente, Paciente.class);
        pacientes.setIdpaciente(pacientes.getIdpaciente()); 
        pacientes.setNombre(pacientes.getNombre());
        pacientes.setDocumento(pacientes.getDocumento());
        pacientes.setEmail(pacientes.getEmail());
        pacientes.setTelefono(pacientes.getTelefono());
        pacientes.setDireccion(pacientes.getDireccion());
        pacientes.setEps(pacientes.getEps());
        pacientes.Guardar(); 
        return new Gson().toJson(pacientes);
    }
    
    // PathVarible sirve para recibir un Id y es para eliminar
    @DeleteMapping("/eliminarCita_medica/{idcita_medica}")
    public String borrarCitamedica(@PathVariable("idcita_medica") int idcita_medica) throws SQLException, ClassNotFoundException {
        c.setIdcita_medica(idcita_medica);
        c.borrar();
        return "Los datos del id indicado han sido eliminados";
    }
}
