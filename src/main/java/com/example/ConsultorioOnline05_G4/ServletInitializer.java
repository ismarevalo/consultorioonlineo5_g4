package com.example.ConsultorioOnline05_G4;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
/**
 *
 * @autores Germán Hernández e Ismael Arévalo - Versión final
 */
public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ConsultorioOnline05G4Application.class);
	}

}
