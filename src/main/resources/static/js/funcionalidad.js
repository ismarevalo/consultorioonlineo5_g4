// JavaScript código fuente
//  @autores Germán Hernández e Ismael Arévalo - Versión final

angular.module('consultorio',[])
.controller('filasPacientes',function($scope,$http){
    // Nos ayuda hacer dinamica la página e ir agregando datos
    // de forma automatica.
    
    $scope.nombre = "";
    $scope.documento = 0;
    $scope.email = "";
    $scope.telefono = "";
    $scope.direccion = "";
    $scope.eps = "";
    
    // Accion del boton consultar
    $scope.consultar = function(){
        if($scope.idpaciente === undefined || $scope.idpaciente === null){
            $scope.idpaciente = 0;
        }
        // Hacemos una petición al servidor
        $http.get("/consultorio/Paciente?idpaciente="+$scope.idpaciente).then(function(data){
            console.log(data.data); // impresión por consola
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.documento;
            $scope.correo = data.data.email;
            $scope.correo = data.data.telefono;
            $scope.correo = data.data.direccion;
            $scope.correo = data.data.eps;
        },function(){
            //error
            $scope.nombre = "";
            $scope.documento = 0;
            $scope.email = "";
            $scope.telefono = "";
            $scope.direccion = "";
            $scope.eps = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/consultorio/Citamedica?idcita_medica="+$scope.idcita_medica).then(function(data){
            console.log(data.data); // mensaje en consola
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.idpaciente === undefined || $scope.idpaciente === null){
            $scope.cedula = 0;
        }
        data = {
            "id": $scope.idpaciente,
            "nombre":$scope.nombre,
            "docuemento": $scope.documento,
            "email": $scope.email,
            "telefono": $scope.telefono,
            "direccion": $scope.direccion,
            "eps": $scope.eps
        };
        $http.post('/consultorio/Paciente', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.documento;
            $scope.correo = data.data.email;
            $scope.correo = data.data.telefono;
            $scope.correo = data.data.direccion;
            $scope.correo = data.data.eps;
        },function(){
            //error
            $scope.nombre = "";
            $scope.documento = 0;
            $scope.email = "";
            $scope.telefono = "";
            $scope.direccion = "";
            $scope.eps = "";
            $scope.filas = [];
        });
        
    };
    
    //acción del botón actualizar
    $scope.guardar = function(){
        // Creamos el JSON
        data = {
            "nombre":$scope.nombre1,
            "docuemento": $scope.documento1,
            "email": $scope.email1,
            "telefono": $scope.telefono1,
            "direccion": $scope.direccion1,
            "eps": $scope.eps1
        };
        $http.post('/consultorio/GuardarPaciente', data).then(function(data){
            //success
            $scope.nombre1 = data.data.nombre1;
            $scope.edad1 = data.data.documento1;
            $scope.correo1 = data.data.email1;
            $scope.correo1 = data.data.telefono1;
            $scope.correo1 = data.data.direccion1;
            $scope.correo1 = data.data.eps1;
        },function(){
            //error
            $scope.nombre1 = "";
            $scope.documento1 = 0;
            $scope.email1 = "";
            $scope.telefono1 = "";
            $scope.direccion1 = "";
            $scope.eps1 = "";
            $scope.filas1 = [];
        });
        
    };
    
    
    // acción del botón borrar
    $scope.borrar = function(){
        if($scope.idcita_medica === undefined || $scope.idcita_medica === null){
            $scope.idcita_medica = 0;
        }
        // Creamos el JSON vacio
        data = {
            "id": $scope.idcuenta
        };
        $http.delete('/consultorio/eliminarCita_medica/'+$scope.idcita_medica, data).then(function(data){
            alert("La cita ha sido eliminada");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});
